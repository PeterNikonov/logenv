<?php

    namespace Logenv;

    use \My\MyPdo;
    use \MyPractic\MyQuery;

    use \Logenv\Config;

    class Data {
    /**
     * Инициировать ресурсы бд
     */
    public function Init() {

    $q = "CREATE TABLE IF NOT EXISTS `".  Config::LOG_TABLE ."` (
`id` int(14) NOT NULL AUTO_INCREMENT,
`user` INT(14),
`text` varchar(256) NOT NULL,
`tmst` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,

    KEY (`user`, `tmst`),

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='';";

    MyPdo::get()->query($q);

    }

    /**
     * Зафиксировать действие
     */
    public function Set($user, $text) {
        MyPdo::get()->query((new MyQuery(Config::LOG_TABLE, ['user' => $user,'text' => $text]))->Insert());
    }
}
